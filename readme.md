instalacja symfony:
composer create-project symfony/website-skeleton my-project

po instalacji symfony wykonujemy:
composer require symfony/twig-bundle <- daje mozliwosc renderowania widokow w .twig

po sciagnieciu projektu wykonujemy:
composer install <- instaluje pakiety z composer.json

debugowanie:
composer require --dev symfony/profiler-pack

serwer:
composer require server --dev

instalowanie annotations:
composer req sensio/framework-extra-bundle annot

instalacja bazy danych:
composer require symfony/orm-pack
composer require symfony/maker-bundle --dev
po instalacji bazy danych należy skonfigurować plik .env

dodawanie tabeli do bazy danych:
php bin/console make:entity <- tworzy encje
php bin/console make:migration <- tworzy migracje
php bin/console doctrine:migrations:migrate <- wykonuje wszystkie migracje
