<?php

namespace App\Repository;

use App\Entity\Fiszka;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Fiszka|null find($id, $lockMode = null, $lockVersion = null)
 * @method Fiszka|null findOneBy(array $criteria, array $orderBy = null)
 * @method Fiszka[]    findAll()
 * @method Fiszka[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FiszkaRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Fiszka::class);
    }

//    /**
//     * @return Fiszka[] Returns an array of Fiszka objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Fiszka
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
