<?php

namespace App\Controller;

use App\Entity\Fiszka;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FiszkaController extends Controller
{
    /**
     * @Route("/fiszki/{$id}")
     */
    public function show($id)
    {
        $fiszka = new Fiszka();

        return new Response('Fiszka');
    }

    /**
     * @Route("/fiszki/nowa")
     */
    public function addForm()
    {
        return $this->render('fiszki/add.html.twig');
    }

    /**
     * @Route("/fiszki/nowa/dodaj")
     */
    public function add(Request $request)
    {
        $question = $request->request->get('question');
        $answer = $request->request->get('answer');
        $package = $request->request->get('package');

        $fiszka = new Fiszka();
        $entityManager = $this->getDoctrine()->getManager();

        foreach (['question', 'answer', 'package'] as $value)
            if (!empty($$value)) $fiszka->{'set' . ucfirst($value)}($$value);

        $entityManager->persist($fiszka);
        $entityManager->flush();

        return new Response('Zapisano fiszkę o id = ' . $fiszka->getId());
    }

    /**
     * @Route("/fiszki/usun/{id}")
     */
    public function delete($id)
    {
        return new Response('Usunieto fiszke id= ' . $id);
    }

    /**
     * @Route("/fiszki/edytuj/{id}")
     */
    public function edit($id)
    {
        return new Response('Edycja fiszki id= ' . $id);
    }
}
