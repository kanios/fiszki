<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends Controller
{
    private $msg = 'This is main site:)';

    /**
     * @Route("/")
     */
    public function index()
    {
        return $this->render('main/main.html.twig', [
            'msg' => $this->msg
        ]);
    }
}
